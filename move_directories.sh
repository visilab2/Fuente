#! /bin/bash
# move.sh -help para mostrar todos las opciones disponibles
#Realizado por Javier Parra Patiño

function default() {
	directorios=`ls -l | grep -v VistaPreliminar`
	`mkdir -p VistaPreliminar`
	`echo "This is the Github repository for project Eyes of Things. Please contact us." > README.md`

	for i in $directorios
	do
		if [ -d $i ]; then
			rsync -rf '+ */' -f '- *' $i VistaPreliminar #Por defecto ya borra las .git (si da fallos -->-f '- .git/' )
		fi
	done

	`find VistaPreliminar/ -type d -empty -exec cp README.md {}/README.md \;`
	`rm README.md`
}

function upRepo() {
	echo "Clonando el repositorio..."
	git clone $repositorio

	echo "Añadiendo el directorio..."

	github=`ls -dt */ | head -n 1`
	contenedor=`ls VistaPreliminar`
	# cd $github
  # `ls -d */ | xargs rm -rf` #Borramos todo lo que contuviera dentro

	for i in $contenedor
	do
		rsync -r VistaPreliminar/$i $github
	done

	cd $github
	git add * -f
	git commit -m "Commit desde move.sh -upRepo"
	git push origin master

	echo "Borrando el directorio clonado..."
	cd ..
	`ls -dt */ | head -n 1 | xargs rm -f -r`
}

function only() {
	`mkdir -p VistaPreliminar`
	`echo "This is the Github repository for project Eyes of Things. Please contact us." > README.md`

	n_dir=$@
	cadena=${n_dir:6} #6 porque son los caracteres de "-only "

	for i in $cadena
	do
		if [ -d $i ]; then
			mkdir -p VistaPreliminar/$i #Necesario, pues rsync no tiene la opcion -p
			rsync -rf '+ */' -f '- *' $i VistaPreliminar/$i #Por defecto ya borra las .git (si da fallos -->-f '- .git/' )
		fi
	done

	`find VistaPreliminar/ -type d -empty -exec cp README.md {}/README.md \;`
	`rm README.md`
}

function onlyRename() {
	`mkdir -p VistaPreliminar`
	`echo "This is the Github repository for project Eyes of Things. Please contact us." > README.md`

	n_dir=$@
	cadena=${n_dir:12}
	vector=(${cadena// / }) #Para transformar el string(cadena) a un array(vector)
	LIMITE=${#vector[@]}

	for ((x=0; x < $LIMITE; x=x+2))
	do
		if [ -d ${vector[$x]} ]; then
			mkdir -p VistaPreliminar/${vector[$x+1]}
			rsync -rf '+ */' -f '- *' ${vector[$x]} VistaPreliminar/${vector[$x+1]} #Por defecto ya borra las .git (si da fallos -->-f '- .git/' )
		fi
	done

	`find VistaPreliminar/ -type d -empty -exec cp README.md {}/README.md \;`
	`rm README.md`
}

function deletePreview() {
	echo "Eliminando el directorio 'VistaPreliminar'..."
	`rm -rf VistaPreliminar`
	echo "Directorio eliminado"
}

function deleteDirsRepo() {
	n_dir=$@
	cadena=${n_dir:15}
	vector=(${cadena// / })
	LIMITE=${#vector[@]}

	echo "Clonando el repositorio..."
	git clone ${vector[0]}

	github=`ls -dt */ | head -n 1`
	cd $github

	for ((x=1; x < $LIMITE; x++))
	do
		if [ -d ${vector[$x]} ]; then
			git rm -rf ${vector[$x]}
		fi
	done

	git add * -f
	git commit -m "Commit desde move.sh -deleteDirsRepo"
	git push origin master

	echo "Todos los directorios seleccionados fueron borrados de GitHub"
	cd ..
	`ls -dt */ | head -n 1 | xargs rm -rf`

}

function renameDirsRepo() {
	n_dir=$@
	cadena=${n_dir:15}
	vector=(${cadena// / })
	LIMITE=${#vector[@]}

	echo "Clonando el repositorio..."
	git clone ${vector[0]}

	github=`ls -dt */ | head -n 1`
	cd $github

	for ((x=1; x < $LIMITE; x=x+2))
	do
		mkdir -p ${vector[$x+1]}
		git mv ${vector[$x]}* ${vector[$x+1]}
		git rm -rf ${vector[$x]}
	done

	git add * -f
	git commit -m "Commit desde move.sh -renameDirsRepo"
	git push origin master

	echo "Borrando el directorio clonado..."
	cd ..
	`ls -dt */ | head -n 1 | xargs rm -f -r`

}

echo ""
if [ $# -eq 0 ]; then #./move.sh
	echo "Copiando todas las carpetas y subcarpetas..."
	default
	echo "Todas las carpetas a subir se encuentran en el directorio 'VistaPreliminar'"

elif [ $1 = "-upRepo" ] && [ $# -gt 1 ]; then #./move.sh -upRepo https://github.com/PruebasVisilab/Destino.git
	echo "Subiendo al repositorio seleccionado..."
	repositorio=$2
	upRepo
	deletePreview

elif [ $1 = "-only" ] && [ $# -gt 1 ]; then #./move.sh -only Animales/Mamiferos/Terrestres/
	echo "Copiando recursivamente todas las carpetas seleccionadas..."
	only $@
	echo "Todas las carpetas seleccionadas para subir se encuentran en el directorio 'VistaPreliminar'"

elif [ $1 = "-onlyRename" ] && [ $# -gt 2 ]; then #./move.sh -onlyRename ReyLeon/Mono/ ReyLeon/Mandril/
	echo "Renombrando los archivos seleccionados...."
	onlyRename $@
	echo "Todas las carpetas seleccionadas y renombradas se encuentran en el directorio 'VistaPreliminar'"

elif [ $1 = "-deletePreview" ]; then #./move.sh -deletePreview
	deletePreview

elif [ $1 = "-deleteDirsRepo" ] && [ $# -gt 2 ]; then #./move.sh -deleteDirsRepo https://github.com/PruebasVisilab/Destino.git Animales/
	echo "Eliminando los directorios seleccionados..."
	deleteDirsRepo $@

elif [ $1 = "-renameDirsRepo" ] && [ $# -gt 3 ]; then #./move.sh -renameDirsRepo https://github.com/PruebasVisilab/Destino.git ReyLeon/Mono/ ReyLeon/Mandril/
	echo "Renombrando los directorios seleccionados..."
	renameDirsRepo $@

elif [ $1 = "-help" ]; then #./move.sh -help
	echo "./move.sh ----------------------------------------------------------------------------------> copia todos los directorios de este directorio y los copia en 'VistaPreliminar'"
	echo "./move.sh -upRepo <Web URL del directorio GitHub> ------------------------------------------> sube todas las carpetas contenidas en 'VistaPreliminar' en el directorio GitHub especificado. No sobrescribe"
	echo "./move.sh -only <Directorio a subir>/ ... --------------------------------------------------> copia los directorios especificados y los guarda en 'VistaPreliminar'"
	echo "./move.sh -onlyRename <Nombre antiguo del directorio>/ <Nombre nuevo del directorio>/ ... --> copia los directorios especificados renombrandolos según el nombre indicado. Guardados en 'VistaPreliminar'"
	echo "./move.sh -deletePreview -------------------------------------------------------------------> borra la carpeta 'VistaPreliminar' y todo su contenido"
	echo "./move.sh -deleteDirsRepo  <Web URL GitHub> <Directorio a borrar>/ ... -------------------------> borra el directorio indicado del repositorio GitHub"
	echo "./move.sh -renameDirsRepo <Web URL GitHub> <Antiguo directorio>/ <Nuevo del directorio>/ ... ---> renombra en el repositoio GitHub los directorios indicados"
	echo "./move.sh -help ----------------------------------------------------------------------------> muestra esta ayuda"
	echo ""
	echo "- Todas las opciones soportan multitud de directorios en la misma ejecución. Por ejemplo --> ./move.sh -only Animales/ ReyLeon/Leon/ "
	echo "- En cada ejecución del programa sólo es válido un modo de funcionamiento. Por ejemplo, no es posible --> ./move.sh -only Animales/ -upRepo https://github.com/PruebasVisilab/Destino.git"

else
	echo "Error! El comando introducido no existe o está mal escrito."
	echo "Por favor, utilice el comando -help para consultar todas las opciones disponibles"
fi

echo ""
exit 0
